#![forbid(unsafe_code)]
#![no_std]

const BITS: usize = core::mem::size_of::<usize>() * 8;
const SHIFT: usize = BITS.trailing_zeros() as usize;
const MASK: usize = BITS - 1;

pub struct Bitmap<S>(S);

impl<S> From<S> for Bitmap<S>
where
    S: AsRef<[usize]>,
{
    fn from(s: S) -> Self {
        Bitmap(s)
    }
}

impl<S> Bitmap<S>
where
    S: AsRef<[usize]>,
{
    pub fn get(&self, n: usize) -> bool {
        self.0.as_ref()[n >> SHIFT] & (1 << (n & MASK)) != 0
    }

    pub fn set(&mut self, n: usize)
    where
        S: AsMut<[usize]>,
    {
        self.0.as_mut()[n >> SHIFT] |= 1 << (n & MASK);
    }

    pub fn unset(&mut self, n: usize)
    where
        S: AsMut<[usize]>,
    {
        self.0.as_mut()[n >> SHIFT] &= !(1 << (n & MASK));
    }
    // todo: {get,set,iter}_range(start..end)
    // todo: bitor bitand bitxor of two bitmaps w\ in-place assign variants

    pub fn find(&self, f: impl Fn(usize, &usize) -> Option<usize>) -> Option<usize> {
        self.0
            .as_ref()
            .iter()
            .enumerate()
            .filter_map(|(i, w)| f(i, w))
            .next()
    }

    pub fn find_first_set(&self) -> Option<usize> {
        self.find(|idx, word| {
            if *word != 0 {
                Some(idx * BITS + word.trailing_zeros() as usize)
            } else {
                None
            }
        })
    }
}

macro_rules! if_feature {
    ($feat:tt, $($i:item)*) => {
        $(
            #[cfg(feature = $feat)]
            $i
        )*
    }
}

if_feature! {"alloc",
    extern crate alloc;

    use crate::alloc::{boxed::Box, vec::Vec, vec};

    impl Bitmap<Box<[usize]>> {
        pub fn with_fixed_capacity(n: usize) -> Self {
            Bitmap(vec![0; (n + MASK) >> SHIFT].into_boxed_slice())
        }
    }

    impl Bitmap<Vec<usize>> {
        pub fn with_capacity(n: usize) -> Self {
            Bitmap(vec![0; (n + MASK) >> SHIFT])
        }

        pub fn resize(&mut self, n: usize) {
            self.0.resize((n + MASK) >> SHIFT, 0);
        }
    }

    #[test]
    fn bitmap_capacity() {
        let mut bm = Bitmap::with_capacity(50);
        bm.set(50);
        assert!(bm.get(50));
        bm.set(63);
        assert!(bm.get(63));
    }

    #[test]
    #[should_panic]
    fn bitmap_capacity_oob() {
        let mut bm = Bitmap::with_capacity(50);
        bm.set(64);
    }
}

#[test]
fn find_first_set() {
    let mut bm = Bitmap::from([0usize; 3]);
    bm.set(72);
    assert_eq!(bm.find_first_set(), Some(72usize));
}
