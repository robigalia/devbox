mod build_idl;

use std::collections::{BTreeMap, BTreeSet};
use std::io::Write;
use std::{env, fs, io, path, process};

use string_morph::Morph;

use proc_macro2::{Ident, Literal, Punct, Spacing, Span, TokenStream};
use quote::*;

#[derive(Clone, Copy)]
enum Bits {
    Bits32,
    Bits64,
}

#[derive(Clone, Copy)]
enum Arch {
    X64,
    AArch64,
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum SyscallGroup {
    Cap,
    Reply,
    Debug,
    DebugPrinter,
    Benchmark,
    Virtualization,
    Yield,
}

enum TypeInfo {
    Enum, // always one word
    Type { bits: u32 },
    Struct { fields: Vec<StructInfo> },
    BitfieldStruct { bits: u32 },
}

struct StructInfo {
    name: String,
    type_: String,
}

// Convert the type from seL4 in the first entry in the tuple to the type in rust in the second
// entry in the tuple
const TYPE_OVERRIDES: &'static [(&'static str, &'static str)] = &[
    ("seL4_CPtr", "CPtr"),
    ("seL4_Word", "usize"),
    ("seL4_Int8", "i8"),
    ("seL4_Int16", "i16"),
    ("seL4_Int32", "i32"),
    ("seL4_Int64", "i64"),
    ("seL4_Uint8", "u8"),
    ("seL4_Uint16", "u16"),
    ("seL4_Uint32", "u32"),
    ("seL4_Uint64", "u64"),
    ("seL4_Bool", "bool"),
];

// Maps a syscall to a syscall group.
// TODO FIXME: This should be in the IDL
const SYSCALL_GROUPS: &'static [(&'static str, SyscallGroup)] = &[
    ("Send", SyscallGroup::Cap),
    ("Recv", SyscallGroup::Cap),
    ("Call", SyscallGroup::Cap),
    ("Reply", SyscallGroup::Reply),
    ("NBSend", SyscallGroup::Cap),
    ("ReplyRecv", SyscallGroup::Reply),
    ("NBRecv", SyscallGroup::Cap),
    ("Yield", SyscallGroup::Yield),
    ("NBSendRecv", SyscallGroup::Cap),
    ("NBSendWait", SyscallGroup::Cap),
    ("Wait", SyscallGroup::Cap),
    ("NBWait", SyscallGroup::Cap),
    ("DebugPutChar", SyscallGroup::DebugPrinter),
    ("DebugDumpScheduler", SyscallGroup::Debug),
    ("DebugHalt", SyscallGroup::Debug),
    ("DebugSnapshot", SyscallGroup::Debug),
    ("DebugCapIdentify", SyscallGroup::Debug),
    ("DebugNameThread", SyscallGroup::Debug),
    ("DebugSendIPI", SyscallGroup::Debug),
    ("DebugRun", SyscallGroup::Debug),
    ("BenchmarkResetLog", SyscallGroup::Benchmark),
    ("BenchmarkFinalizeLog", SyscallGroup::Benchmark),
    ("BenchmarkSetLogBuffer", SyscallGroup::Benchmark),
    ("BenchmarkNullSyscall", SyscallGroup::Benchmark),
    ("BenchmarkFlushCaches", SyscallGroup::Benchmark),
    ("BenchmarkGetThreadUtilisation", SyscallGroup::Benchmark),
    ("BenchmarkResetThreadUtilisation", SyscallGroup::Benchmark),
    ("VMEnter", SyscallGroup::Virtualization),
];

fn main() {
    //
    // Load our files and directories from environment variables and/or default locations
    // and verify that they exist and are valid.
    //

    let cargo_manifest = env::var("CARGO_MANIFEST_DIR")
        .map(path::PathBuf::from)
        .expect("Expected CARGO_MANIFEST_DIR, are you using cargo?");
    eprintln!("cargo_manifest: {}", cargo_manifest.display());

    assert!(
        cargo_manifest.join("sel4").join("libsel4").exists(),
        "Could not find seL4 kernel source at {:?}. Did you forget to git clone recursively? Try: git submodule update --init --recursive",
        cargo_manifest.join("sel4")
    );

    let outdir = env::var("OUT_DIR")
        .map(path::PathBuf::from)
        .expect("expected OUT_DIR, are you using cargo?");
    eprintln!("outdir: {}", outdir.display());

    let sel4_build_dir = outdir.join("build");
    eprintln!("sel4_build_dir: {}", sel4_build_dir.display());

    // The seL4 build dir might exist from a previous compilation. If we don't clear those out we
    // could confuse ourselves.
    if sel4_build_dir.exists() {
        fs::remove_dir_all(&sel4_build_dir).expect("Could not delete sel4 build directory");
    }

    fs::create_dir(&sel4_build_dir).expect("Could not create sel4 build directory");

    let kernel_config = env::var("SEL4_CONFIG").map(path::PathBuf::from).unwrap_or(
        cargo_manifest
            .join("sel4")
            .join("configs")
            .join("config.cmake"),
    );
    eprintln!("kernel_config: {}", kernel_config.display());

    assert!(
        kernel_config.exists(),
        "Could not find seL4 kernel config at {:?}, please create it or pass the path to your kernel config in the SEL4_CONFIG env variable",
        kernel_config
    );

    let cmake_toolchain_file = env::var("CMAKE_TOOLCHAIN_FILE")
        .map(path::PathBuf::from)
        .unwrap_or(cargo_manifest.join("sel4").join("gcc.cmake"));
    eprintln!("cmake_toolchain_file: {}", cmake_toolchain_file.display());

    assert!(
        cmake_toolchain_file.exists(),
        "Could not find cmake toolchain file at {:?}, please create it or pass the path in the CMAKE_TOOLCHAIN_FILE env variable",
        cmake_toolchain_file
    );

    let sel4_version_file = cargo_manifest.join("sel4").join("VERSION");
    eprintln!("sel4_version_file: {}", sel4_version_file.display());

    assert!(
        sel4_version_file.exists(),
        "Could not find seL4 kernel version file at {:?}",
        sel4_version_file
    );

    let sel4_version =
        fs::read_to_string(&sel4_version_file).expect("Could not read seL4 kernel version file");
    eprintln!("sel4_version: {}", sel4_version);

    let sel4_idl_file = cargo_manifest
        .join("idl")
        .join(format!("sel4-{}.xml", &sel4_version));
    eprintln!("sel4_idl_file: {}", sel4_idl_file.display());

    assert!(
        sel4_idl_file.exists(),
        "Could not find seL4 IDL file for this sel4 version at {:?}, please generate it with https://gitlab.com/robigalia/sel4-idlgen",
        sel4_idl_file
    );

    //
    // Tell cargo to watch important environment variables and files and rerun the build if they change
    //

    println!("cargo:rerun-if-env-changed=SEL4_CONFIG");
    println!("cargo:rerun-if-env-changed=CMAKE_TOOLCHAIN_FILE");
    println!("cargo:rerun-if-changed={}", kernel_config.display());
    println!("cargo:rerun-if-changed={}", cmake_toolchain_file.display());
    println!("cargo:rerun-if-changed={}", sel4_version_file.display());
    println!("cargo:rerun-if-changed={}", sel4_idl_file.display());

    //
    // Configure and build seL4
    //

    // configure the kernel
    let cmake_exec = env::var("CMAKE").unwrap_or("cmake".to_owned());
    let status = process::Command::new(&cmake_exec)
        .current_dir(&sel4_build_dir)
        .arg(format!(
            "-DCMAKE_TOOLCHAIN_FILE={}",
            cmake_toolchain_file.display()
        ))
        .arg("-G")
        .arg("Ninja")
        .arg("-C")
        .arg(&kernel_config)
        .arg(cargo_manifest.join("sel4/"))
        .status()
        .expect("seL4 kernel configure failed");

    assert!(status.success(), "seL4 kernel configure failed");

    // build the kernel
    let ninja_exec = env::var("NINJA").unwrap_or("ninja".to_owned());
    let status = process::Command::new(&ninja_exec)
        .current_dir(&sel4_build_dir)
        .arg("kernel.elf")
        .status()
        .expect("sel4 kernel build failed");

    assert!(status.success(), "seL4 kernel build failed");

    //
    // Open the output file that we will store all of our generated code into
    //

    let mut out = fs::File::create(outdir.join("generated.rs"))
        .map(io::BufWriter::new)
        .expect("failed to open file out/generated.rs");

    //
    // Read the generated seL4 kernel configuration C #defines and:
    // - Store them as constants in our generated code
    // - Set corresponding rust feature flags
    //

    let mut arch = None;
    let mut word_size = None;
    let mut bug_dup_wcet_scale = false;

    for line in fs::read_to_string(
        sel4_build_dir
            .join("gen_config")
            .join("kernel")
            .join("gen_config.h"),
    )
    .expect("failed to read gen_config.h")
    .lines()
    .map(|l| l.trim_start_matches("#define "))
    {
        let space = line.find(" ").expect("failed to find space!");
        let name = &line[..space];
        let value = &line[space + 1..];
        let iname = Ident::new(name, Span::call_site());

        // add key=value to the environment
        println!(r#"cargo:rustc-cfg=feature="{}={}""#, name, value);

        if name == "CONFIG_KERNEL_WCET_SCALE" {
            if bug_dup_wcet_scale {
                continue;
            }
            bug_dup_wcet_scale = true;
        }

        // FIXME: This will fail when compiling for a 64-bit platform an a 32-bit system
        if let Ok(int) = value.parse::<usize>() {
            write(&mut out, quote!( pub const #iname: usize = #int; ));

            // probably 1 = true; if not, oh well
            if int == 1 {
                println!(r#"cargo:rustc-cfg=feature="{}""#, name);
            }

            // add special cases for any integer comparisons
            match name {
                "CONFIG_MAX_NUM_NODES" if int > 0 => {
                    println!(r#"cargo:rustc-cfg=feature="CONFIGX_ENABLE_SMP_SUPPORT""#);
                }
                "CONFIG_WORD_SIZE" if int == 32 => {
                    word_size = Some(Bits::Bits32);
                }
                "CONFIG_WORD_SIZE" if int == 64 => {
                    word_size = Some(Bits::Bits64);
                }
                "CONFIG_ARCH_X86_64" => {
                    arch = Some(Arch::X64);
                }
                "CONFIG_ARCH_AARCH64" => {
                    arch = Some(Arch::AArch64);
                }
                _ => (),
            }
        } else {
            write(&mut out, quote!( pub const #iname: &str = #value; ));
        }
    }

    let arch = arch.expect(
        "Unknown architecture selected in seL4 config. Currently only support X86_64 and AArch64.",
    );
    let word_size = word_size.expect("CONFIG_WORD_SIZE not set in seL4 config");

    //
    // lib.rs contains some special notation in comments that affect our code generation behavior.
    // This allows lib.rs to override code that we generate with a hand-written version. Note that
    // we still do code generation for overidden things, we just give our generated version an
    // internal name and export the hand-written version. The hand-written version thus still has
    // access to the generated code and can wrap it.
    //

    let lib_rs = fs::read_to_string(cargo_manifest.join("src").join("lib.rs"))
        .expect("failed to read src/lib.rs");
    let overrides = lib_rs
        .lines()
        .map(|l| l.trim())
        .filter_map(|line| match line {
            _ if line.starts_with("// OVERRIDE: ") => {
                let from = line.trim_start_matches("// OVERRIDE: ");
                let to = format!(
                    "{}_internal",
                    &from.split_at(from.find("::").unwrap()).1[2..]
                );
                Some((from, to))
            }
            _ if line.starts_with("// RENAME: ") => {
                let tmp = line.trim_start_matches("// RENAME: ");
                let (from, to) = tmp.split_at(tmp.find("=").unwrap());
                Some((from.trim(), to[1..].trim().to_owned()))
            }
            _ => None,
        })
        .collect::<BTreeMap<_, _>>();

    //
    // Generate code from the seL4 IDL file generated by https://gitlab.com/robigalia/sel4-idlgen
    //

    // Remember what size each bitfield struct is so that tagged unions can look them up
    let mut bitfield_size_in_bits = BTreeMap::new();
    // Remember information about each type so we can figure out how to serialize and deserialize
    // it if it gets used in an invocation
    let mut type_info = BTreeMap::new();

    for entry in build_idl::Sel4Idl::new(
        &fs::read_to_string(sel4_idl_file).expect("Could not read seL4 idl file"),
    )
    .eval(&sel4_build_dir)
    {
        match entry {
            build_idl::Entry::Constant { name, value } => {
                write(
                    &mut out,
                    match value {
                        build_idl::Value::Uint(v) => format!(
                            "pub const {}: usize = {};",
                            rustify_name(name).to_uppercase(),
                            v
                        ),
                        build_idl::Value::Sint(v) => format!(
                            "pub const {}: usize = {}isize as usize;",
                            rustify_name(name).to_uppercase(),
                            v
                        ),
                        build_idl::Value::Null => panic!(
                            "Null value found for constant '{}' while evaluating IDL file",
                            name
                        ),
                        build_idl::Value::Undefined => panic!(
                            "Undefined value found for constant '{}' while evaluating IDL file",
                            name
                        ),
                    },
                );
            }
            build_idl::Entry::Enum { name, constants } => {
                // Remember some information about us so that the invocation layer can emit
                // serialization and deserialization code
                type_info.insert(name, TypeInfo::Enum);

                let id_enum_name = ident(&rustify_type(name, word_size));
                let mut previous_values = BTreeSet::new();
                let mut overflow = TokenStream::new();

                let constants = constants.iter().map(|c| {
                    let id_name = ident(&rustify_type(c.name, word_size));

                    let expr_value = match c.value {
                        // FIXME: This will fail when compiling for a 32-bit platform
                        build_idl::Value::Uint(v) => quote!( #v as usize ),
                        build_idl::Value::Sint(v) => quote!( #v as usize ),
                        build_idl::Value::Null => panic!(
                            "Null value found for enum '{}' constant '{}' while evaluating IDL file",
                            name, c.name,
                            ),
                        build_idl::Value::Undefined => panic!(
                            "Undefined value found for enum '{}' constant '{}' while evaluating IDL file",
                            name, c.name,
                            ),
                    };

                    let v_u64 = c.value.as_u64().unwrap();

                    if previous_values.contains(&v_u64) {
                        // HACK: C enums can contain multiple constants with the same value, but
                        // rust enums cannot. If we encounter this situation, the enum constant
                        // will be promoted to a pub const, at least until we figure out a better
                        // way to handle this.
                        overflow.extend(quote!( pub const #id_name : usize = #expr_value; ));
                        quote!()
                    } else {
                        previous_values.insert(v_u64);
                        quote!( #id_name = #expr_value, )
                    }
                });

                write(
                    &mut out,
                    quote!(
                        #[derive(Debug)]
                        #[repr(usize)]
                        pub enum #id_enum_name {
                            #( #constants )*
                        }
                    ),
                );

                write(&mut out, overflow);
            }
            build_idl::Entry::Type {
                name,
                type_,
                size_in_bytes,
            } => {
                // Remember some information about us so that the invocation layer can emit
                // serialization and deserialization code
                type_info.insert(
                    name,
                    TypeInfo::Type {
                        bits: size_in_bytes as u32 * 8,
                    },
                );

                if let Some(_) = get_type_override(name) {
                    // If we are manually replacing all occurances of this type with a specific rust
                    // type then we don't need a type definition for it
                    continue;
                }

                let id_name = ident(&rustify_type(name, word_size));
                let id_type = ident(&rustify_type(type_, word_size));

                if type_ == "seL4_CPtr" {
                    write(
                        &mut out,
                        quote!(
                            pub struct #id_name(Cap);

                            impl AsRef<Cap> for #id_name {
                                fn as_ref(&self) -> &Cap {
                                    &self.0
                                }
                            }

                            impl From<Cap> for #id_name {
                                fn from(c: Cap) -> Self {
                                    #id_name(c)
                                }
                            }
                        ),
                    );
                } else {
                    write(&mut out, quote!( pub type #id_name = #id_type; ));
                }
            }
            build_idl::Entry::Struct {
                name,
                is_packed,
                fields,
            } => {
                // Remember some information about us so that the invocation layer can emit
                // serialization and deserialization code
                if fields
                    .iter()
                    .filter(|f| {
                        if let build_idl::StructFieldType::Simple(_) = f.type_ {
                            false
                        } else {
                            true
                        }
                    })
                    .count()
                    == 0
                {
                    // We only support simple types for now
                    type_info.insert(
                        name,
                        TypeInfo::Struct {
                            fields: fields
                                .iter()
                                .map(|f| StructInfo {
                                    name: f.name.to_owned(),
                                    type_: match f.type_ {
                                        build_idl::StructFieldType::Simple(t) => t.to_owned(),
                                        build_idl::StructFieldType::Pointer(_) => unimplemented!(),
                                        build_idl::StructFieldType::Array(_, _) => unimplemented!(),
                                    },
                                })
                                .collect(),
                        },
                    );
                }
                if name == "seL4_IPCBuffer" {
                    // We will rename seL4's version of the IPCBuffer as RawIPCBuffer. We define
                    // our own IPCBuffer struct in src/ipcbuffer.rs. We do some special handling
                    // here to make accessor functions to poke at the real IPCBuffer from our
                    // wrapper struct.

                    let field_accessors = fields.iter().map(|field| {
                        let field_name = ident(&rustify_name(field.name));
                        match field.type_ {
                            build_idl::StructFieldType::Simple(t) => {
                                let field_type = if field.name == "receiveIndex" {
                                    // HACK: Wrong type for this field in IPC buffer.
                                    // TODO This should be fixed upstream
                                    ident("usize")
                                } else {
                                    ident(&rustify_type(t, word_size))
                                };
                                let get_fn = ident(&format!("get_{}", rustify_name(field.name)));
                                let set_fn = ident(&format!("set_{}", rustify_name(field.name)));
                                quote!(
                                    #[inline(always)]
                                    pub unsafe fn #get_fn(&self) -> #field_type {
                                        (*self.0).#field_name
                                    }

                                    #[inline(always)]
                                    pub unsafe fn #set_fn(&mut self, #field_name: #field_type) {
                                        (*self.0).#field_name = #field_name;
                                    }
                                )
                            }
                            build_idl::StructFieldType::Pointer(_t) => {
                                panic!("Unhandled pointer field accessor in IPCBuffer");
                            }
                            build_idl::StructFieldType::Array(t, v) => {
                                let field_type = ident(&rustify_type(t, word_size));
                                let get_fn = ident(&format!("get_{}", rustify_name(field.name)));
                                let set_fn = ident(&format!("set_{}", rustify_name(field.name)));
                                let get_ref_fn =
                                    ident(&format!("get_{}_ref", rustify_name(field.name)));
                                let get_mut_fn =
                                    ident(&format!("get_{}_mut", rustify_name(field.name)));
                                let v = v.as_u64().expect(
                                    "Fixed length array size in struct field has non-numeric value",
                                );
                                quote!(
                                    #[inline(always)]
                                    pub unsafe fn #get_fn(&self) -> [#field_type; #v as usize] {
                                        (*self.0).#field_name
                                    }

                                    #[inline(always)]
                                    pub unsafe fn #set_fn(&mut self, #field_name: [#field_type; #v as usize]) {
                                        (*self.0).#field_name = #field_name;
                                    }

                                    #[inline(always)]
                                    pub unsafe fn #get_ref_fn(&self) -> &[#field_type; #v as usize] {
                                        &(*self.0).#field_name
                                    }

                                    #[inline(always)]
                                    pub unsafe fn #get_mut_fn(&mut self) -> &mut [#field_type; #v as usize] {
                                        &mut (*self.0).#field_name
                                    }
                                )
                            }
                        }
                    });

                    write(
                        &mut out,
                        quote!(
                            impl IPCBuffer {
                                #( #field_accessors )*
                            }
                        ),
                    );
                }

                // Normal struct processing

                let id_name = ident(&rustify_type(name, word_size));

                let packed = if is_packed {
                    quote!( ,packed )
                } else {
                    quote!()
                };

                let fields = fields.iter().map(|field| {
                    let field_name = ident(&rustify_name(field.name));
                    match field.type_ {
                        build_idl::StructFieldType::Simple(t) => {
                            let field_type =
                                if name == "seL4_IPCBuffer" && field.name == "receiveIndex" {
                                    // HACK: Wrong type for this field in IPC buffer.
                                    // TODO This should be fixed upstream
                                    ident("usize")
                                } else {
                                    ident(&rustify_type(t, word_size))
                                };
                            quote!( pub #field_name: #field_type, )
                        }
                        build_idl::StructFieldType::Pointer(t) => {
                            let field_type = ident(&rustify_type(t, word_size));
                            quote!( pub #field_name: *mut #field_type, )
                        }
                        build_idl::StructFieldType::Array(t, v) => {
                            let field_type = ident(&rustify_type(t, word_size));
                            let v = v.as_u64().expect(
                                "Fixed length array size in struct field has non-numeric value",
                            );
                            // FIXME: This will fail when compiling for a 32-bit platform
                            quote!( pub #field_name: [#field_type; #v as usize], )
                        }
                    }
                });

                write(
                    &mut out,
                    quote!(
                        #[repr(C #packed)]
                        pub struct #id_name {
                            #( #fields )*
                        }
                    ),
                );
            }
            build_idl::Entry::BitfieldStruct { name, fields } => {
                let id_name = ident(&rustify_type(name, word_size));
                let id_builder_name = ident(&format!("{}Builder", rustify_type(name, word_size)));
                let word_bits = match word_size {
                    Bits::Bits32 => 32,
                    Bits::Bits64 => 64,
                };
                let struct_bits = fields.iter().fold(0, |sum, f| sum + f.num_bits);

                // Remember some information about us so that the invocation layer can emit
                // serialization and deserialization code
                type_info.insert(
                    name,
                    TypeInfo::BitfieldStruct {
                        bits: struct_bits as u32,
                    },
                );

                assert!(
                    struct_bits % word_bits == 0,
                    "Could not process bitfield struct '{}' because its size '{} bits' is not a multiple of the target word size '{} bits'",
                    name, struct_bits, word_bits,
                );

                bitfield_size_in_bits.insert(name, struct_bits);

                let num_words = struct_bits / word_bits;

                let mut field_accessors = TokenStream::new();
                let mut builder_methods = TokenStream::new();
                // fields allocate bits from the last bit to the first bit
                let mut cur_bit = struct_bits;

                for f in fields {
                    if let Some(field_name) = f.name {
                        let id_field_name = ident(&rustify_name(field_name));
                        let get_fn_name = ident(&format!("get_{}", rustify_name(field_name)));
                        let set_fn_name = ident(&format!("set_{}", rustify_name(field_name)));

                        // Allocate this field's bits
                        assert!(
                            cur_bit >= f.num_bits,
                            "Bitfield struct '{}' field '{:?}' used too many bits",
                            name,
                            f.name,
                        );
                        cur_bit -= f.num_bits;

                        let cur_word = cur_bit / word_bits;
                        let bit_pos = cur_bit % word_bits;

                        assert!(
                            cur_word == (cur_bit + f.num_bits - 1) / word_bits,
                            "Bitfield struct '{}' field '{:?}' spans multiple '{} bits' words",
                            name,
                            f.name,
                            word_bits,
                        );

                        let bitmask = {
                            let mut ret = 0u64;
                            for i in bit_pos..bit_pos + f.num_bits {
                                ret |= 1 << i;
                            }
                            ret
                        };

                        let array_index = if num_words > 1 {
                            quote!( [#cur_word as usize] )
                        } else {
                            quote!()
                        };

                        field_accessors.extend(quote!(
                            #[inline(always)]
                            pub const fn #get_fn_name(&self) -> usize {
                                (self.0#array_index & (#bitmask as usize)) >> #bit_pos as usize
                                // TODO FIXME: Possibly sign extend
                            }

                            #[inline(always)]
                            pub fn #set_fn_name(&mut self, v: usize) {
                                // TODO: Assert input is in valid range
                                self.0#array_index &= !(#bitmask as usize);
                                self.0#array_index |= (v << (#bit_pos as usize)) & (#bitmask as usize);
                            }
                        ));

                        builder_methods.extend(quote!(
                            #[inline(always)]
                            pub const fn #id_field_name(mut self, v: usize) -> Self {
                                // TODO: Assert input is in valid range
                                self.0#array_index &= !(#bitmask as usize);
                                self.0#array_index |= (v << (#bit_pos as usize)) & (#bitmask as usize);
                                self
                            }
                        ));
                    } else {
                        // Just padding
                        cur_bit -= f.num_bits;
                    }
                }

                let type_definition = if num_words > 1 {
                    quote!( [usize; #num_words as usize] )
                } else {
                    quote!(usize)
                };

                let type_initialization = if num_words > 1 {
                    quote!( [0; #num_words as usize] )
                } else {
                    quote!(0)
                };

                write(
                    &mut out,
                    quote!(
                        #[derive(Clone, Copy)]
                        #[repr(transparent)]
                        pub struct #id_name(#type_definition);

                        impl #id_name {
                            #[inline(always)]
                            pub const fn new() -> #id_builder_name {
                                #id_builder_name(#type_initialization)
                            }

                            #[inline(always)]
                            pub const unsafe fn from_raw(raw: #type_definition) -> Self {
                                Self(raw)
                            }

                            #[inline(always)]
                            pub const fn into_raw(self) -> #type_definition {
                                self.0
                            }

                            #[inline(always)]
                            pub const fn as_raw(&self) -> #type_definition {
                                self.0
                            }

                            #field_accessors
                        }

                        #[repr(transparent)]
                        pub struct #id_builder_name(#type_definition);

                        impl #id_builder_name {
                            #[inline(always)]
                            pub const fn build(self) -> #id_name {
                                unsafe { #id_name::from_raw(self.0) }
                            }

                            #builder_methods
                        }
                    ),
                );

                assert!(
                    cur_bit == 0,
                    "Bitfield struct '{}' did not use all of its bits",
                    name
                );
            }
            build_idl::Entry::TaggedUnion {
                union_name,
                tag_name,
                fields,
            } => {
                let id_enum_name = ident(&rustify_type(union_name, word_size));
                let id_into_enum = ident(&format!("into_{}", rustify_name(union_name)));
                let id_as_enum = ident(&format!("as_{}", rustify_name(union_name)));
                let id_get_tag_name = ident(&format!("get_{}", rustify_name(tag_name)));
                let id_first_type_name = ident(&rustify_type(fields[0].type_, word_size));
                let size_in_bits = bitfield_size_in_bits.get(fields[0].type_).unwrap();
                let word_bits = match word_size {
                    Bits::Bits32 => 32,
                    Bits::Bits64 => 64,
                };
                let num_words = size_in_bits / word_bits;

                let enum_field_defs = fields.iter().map(|field| {
                    let id_field_type = ident(&rustify_type(field.type_, word_size));
                    quote!(#id_field_type(#id_field_type),)
                });

                let enum_constructors = fields.iter().map(|field| {
                    let id_field_type = ident(&rustify_type(field.type_, word_size));
                    let tag_constant = Literal::u64_unsuffixed(field.tag_constant);
                    quote!(#tag_constant => Some(Self::#id_field_type(#id_field_type(words))),)
                });

                let enum_into_raw = fields.iter().map(|field| {
                    let id_field_type = ident(&rustify_type(field.type_, word_size));
                    quote!(Self::#id_field_type(bf) => bf.into_raw(),)
                });

                let enum_as_raw = fields.iter().map(|field| {
                    let id_field_type = ident(&rustify_type(field.type_, word_size));
                    quote!(Self::#id_field_type(bf) => bf.as_raw(),)
                });

                let bitfield_struct_impls = fields.iter().map(|field| {
                    let id_field_type = ident(&rustify_type(field.type_, word_size));
                    quote!(
                        impl #id_field_type {
                            pub const fn #id_into_enum(self) -> #id_enum_name {
                                #id_enum_name::#id_field_type(self)
                            }
                            pub const fn #id_as_enum(&self) -> #id_enum_name {
                                #id_enum_name::#id_field_type(*self)
                            }
                        }
                    )
                });

                let type_definition = if num_words > 1 {
                    quote!( [usize; #num_words as usize] )
                } else {
                    quote!(usize)
                };

                write(
                    &mut out,
                    quote!(
                        pub enum #id_enum_name {
                            #( #enum_field_defs )*
                        }

                        impl #id_enum_name {
                            pub const unsafe fn from_raw(words: #type_definition) -> Option<Self> {
                                // This is safe because each bitfield in the union is guaranteed to
                                // have the same number of bits and have the tag located in the
                                // same place
                                let temp = #id_first_type_name::from_raw(words);
                                match temp.#id_get_tag_name() {
                                    #( #enum_constructors )*
                                    _ => None,
                                }
                            }

                            pub const fn into_raw(self) -> #type_definition {
                                match self {
                                    #( #enum_into_raw )*
                                }
                            }

                            pub const fn as_raw(&self) -> #type_definition {
                                match *self {
                                    #( #enum_as_raw )*
                                }
                            }

                            pub const fn num_words() -> usize {
                                #num_words as usize
                            }
                        }

                        #( #bitfield_struct_impls )*
                    ),
                );
            }
            build_idl::Entry::Syscall {
                name,
                number,
                does_send_ipcbuf,
                does_receive_ipcbuf,
                arguments,
                return_values,
            } => {
                // TODO:
                //     - need to honor syscall/sysenter config flag on x64
                //     - support more architectures

                let group = get_syscall_group(name);
                let (name, id_group) = match group {
                    SyscallGroup::Cap => (name, ident("Cap")),
                    SyscallGroup::Reply => (name, ident("Reply")),
                    SyscallGroup::Debug => (name.trim_start_matches("Debug"), ident("Debug")),
                    SyscallGroup::DebugPrinter => {
                        (name.trim_start_matches("Debug"), ident("DebugPrinter"))
                    }
                    SyscallGroup::Benchmark => {
                        (name.trim_start_matches("Benchmark"), ident("Benchmark"))
                    }
                    SyscallGroup::Virtualization => (name, ident("Virtualization")),
                    SyscallGroup::Yield => (name, ident("Yield")),
                };

                // Controls special handling for the case where one of our arguments is passed as
                // the self parameter
                let (self_arg_name, self_arg_decl) = if group == SyscallGroup::Cap {
                    let arg = arguments
                        .iter()
                        .filter(|x| x.type_ == "seL4_CPtr")
                        .nth(0)
                        .expect("Syscall in cap group expects argument with type seL4_CPtr");
                    let id_name = ident(&rustify_name(arg.name));
                    (Some(arg.name), quote!( let #id_name = self.as_cptr(); ))
                } else {
                    (None, quote!())
                };

                let syscall_name = if does_send_ipcbuf || does_receive_ipcbuf {
                    // We generate two stubs for IPC-related syscalls, a higher-level function and
                    // a low-level _raw function
                    ident(&format!("{}_raw", rustify_name(name)))
                } else {
                    ident(&rustify_name(name))
                };

                // high-level syscall

                if does_send_ipcbuf || does_receive_ipcbuf {
                    let syscall_name_not_raw = ident(&rustify_name(name));
                    let receive_vars = return_values.iter().map(|v| {
                        let id_name = ident(&rustify_name(&format!("out_{}", v.name)));
                        quote!(#id_name,)
                    });
                    let receive_vars_only_mrs = return_values
                        .iter()
                        .filter(|v| v.name.starts_with("mr"))
                        .map(|v| {
                            let id_name = ident(&rustify_name(&format!("out_{}", v.name)));
                            quote!(#id_name,)
                        });
                    let send_vars_no_msginfo_or_mrs = arguments
                        .iter()
                        .filter(|v| {
                            !v.name.starts_with("mr") && v.name != "info" && v.name != "sysno"
                        })
                        .filter(|v| {
                            if let Some(n) = self_arg_name {
                                v.name != n
                            } else {
                                true
                            }
                        })
                        .map(|v| {
                            let id_name = ident(&rustify_name(v.name));
                            let id_type = ident(&rustify_type(v.type_, word_size));
                            quote!(#id_name : #id_type,)
                        });
                    let call_args = arguments
                        .iter()
                        .filter(|v| v.name != "sysno")
                        .filter(|v| {
                            if let Some(n) = self_arg_name {
                                v.name != n
                            } else {
                                true
                            }
                        })
                        .map(|v| match v.name {
                            "info" => quote!(message.tag,),
                            x if x.starts_with("mr") => {
                                let num: u32 = x[2..].parse().expect("message register number");
                                quote!(message.cpureg[#num as usize],)
                            }
                            x => {
                                let id_name = ident(&rustify_name(x));
                                quote!(#id_name,)
                            }
                        });
                    let out_badge = return_values
                        .iter()
                        .filter(|v| v.name == "badge")
                        .map(|v| {
                            let id_name = ident(&rustify_name(&format!("out_{}", v.name)));
                            quote!(#id_name)
                        })
                        .nth(0)
                        .unwrap_or(quote!(0 as usize));

                    if does_send_ipcbuf && does_receive_ipcbuf {
                        let (return_type, make_ret_a_call_reply) = if name == "Call" {
                            (
                                quote!(IPCMessageInCallReply),
                                quote!( let ret = IPCMessageInCallReply(ret); ),
                            )
                        } else {
                            (quote!(IPCMessageIn), quote!())
                        };
                        write(
                            &mut out,
                            quote!(
                                impl #id_group {
                                    #[inline(always)]
                                    pub unsafe fn #syscall_name_not_raw<'ipcbuf>(&self, #( #send_vars_no_msginfo_or_mrs )* message: IPCMessageOut<'ipcbuf>) -> #return_type<'ipcbuf> {
                                        let (#( #receive_vars )*) = self.#syscall_name(message.ipcbuf, #( #call_args )*);

                                        let ret = IPCMessageIn::new(message.ipcbuf, #out_badge, out_info, [#( #receive_vars_only_mrs )*]);
                                        #make_ret_a_call_reply
                                        ret
                                    }
                                }
                            ),
                        );
                    } else if does_send_ipcbuf {
                        write(
                            &mut out,
                            quote!(
                                impl #id_group {
                                    #[inline(always)]
                                    pub unsafe fn #syscall_name_not_raw<'ipcbuf>(&self, #( #send_vars_no_msginfo_or_mrs )* message: &IPCMessageOut<'ipcbuf>) {
                                        self.#syscall_name(message.ipcbuf, #( #call_args )*);
                                    }
                                }
                            ),
                        );
                    } else if does_receive_ipcbuf {
                        write(
                            &mut out,
                            quote!(
                                impl #id_group {
                                    #[inline(always)]
                                    pub unsafe fn #syscall_name_not_raw<'ipcbuf>(&self, ipcbuf: &'ipcbuf mut IPCBuffer, #( #send_vars_no_msginfo_or_mrs )*) -> IPCMessageIn<'ipcbuf> {
                                        let (#( #receive_vars )*) = self.#syscall_name(ipcbuf, #( #call_args )*);

                                        IPCMessageIn::new(ipcbuf, out_badge, out_info, [#( #receive_vars_only_mrs )*])
                                    }
                                }
                            ),
                        );
                    }
                }

                // low-level syscall

                let syscall_number = number.as_i64().unwrap() as isize;

                let mut input_params = TokenStream::new();
                let mut input_asm_block = TokenStream::new();
                let mut has_sysno = false;

                match (does_send_ipcbuf, does_receive_ipcbuf) {
                    (true, true) => input_params.extend(quote!(_ipcbuf: &mut IPCBuffer,)),
                    (false, true) => input_params.extend(quote!(_ipcbuf: &mut IPCBuffer,)),
                    (true, false) => input_params.extend(quote!(_ipcbuf: &IPCBuffer,)),
                    (false, false) => {}
                }

                for param in arguments {
                    let id_name = ident(&rustify_name(param.name));
                    let id_type = ident(&rustify_type(param.type_, word_size));
                    let reg = format!("{{{}}}", param.register);

                    match param.special_value {
                        Some("sysno") => {
                            has_sysno = true;
                            if input_asm_block.is_empty() {
                                input_asm_block.extend(quote!( #reg (#syscall_number as usize) ));
                            } else {
                                input_asm_block.extend(quote!( , #reg (#syscall_number as usize) ));
                            }
                        }
                        None => {
                            if let Some(n) = self_arg_name {
                                // If the arg is passed as self, don't include it in the
                                // formal parameter list
                                if n != param.name {
                                    input_params.extend(quote!( #id_name: #id_type, ));
                                }
                            } else {
                                input_params.extend(quote!( #id_name: #id_type, ));
                            }
                            if input_asm_block.is_empty() {
                                input_asm_block.extend(quote!( #reg (#id_name) ));
                            } else {
                                input_asm_block.extend(quote!( , #reg (#id_name) ));
                            }
                        }
                        Some(x) => panic!(
                            "Unknown special value '{}' for syscall '{}' argument '{}'",
                            x, name, param.name
                        ),
                    }
                }

                if !has_sysno {
                    panic!(
                        "IDL: Unspecified system call number register for syscall '{}'",
                        name
                    );
                }

                let mut output_params = TokenStream::new();
                let mut output_asm_block = TokenStream::new();
                let mut output_params_decl = TokenStream::new();
                let mut output_params_return = TokenStream::new();

                for param in &return_values {
                    let id_name = ident(&rustify_name(&format!("out_{}", param.name)));
                    let id_type = ident(&rustify_type(param.type_, word_size));
                    let reg = format!("={{{}}}", param.register);

                    output_params_decl.extend(quote!( let #id_name: #id_type; ));

                    if output_params.is_empty() {
                        output_params.extend(quote!( #id_type ));
                        output_params_return.extend(quote!( #id_name ));
                        output_asm_block.extend(quote!( #reg (#id_name) ));
                    } else {
                        output_params.extend(quote!( , #id_type ));
                        output_params_return.extend(quote!( , #id_name ));
                        output_asm_block.extend(quote!( , #reg (#id_name) ));
                    }

                    if (name == "Recv" || name == "NBRecv" || name == "Wait" || name == "NBWait")
                        && param.name == "info"
                    {
                        // Seed the message info register with an invalid value so after the
                        // receive we can tell whether we received a bound notification or an IPC
                        // message
                        let reg = format!("{{{}}}", param.register);
                        let extend = quote!( #reg (endpoint::BOUND_NOTIFICATION_SENTINEL) );

                        if input_asm_block.is_empty() {
                            input_asm_block.extend(extend);
                        } else {
                            input_asm_block.extend(quote!( , #extend ));
                        }
                    }
                }

                output_params = match return_values.len() {
                    0 => quote!(),
                    1 => quote!( -> #output_params ),
                    _ => quote!( -> (#output_params) ),
                };

                output_params_return = match return_values.len() {
                    0 => quote!(),
                    1 => output_params_return,
                    _ => quote!( (#output_params_return) ),
                };

                let set_msginfo_sentinel = if does_send_ipcbuf
                    && does_receive_ipcbuf
                    && (name.contains("Recv") || name.contains("Wait"))
                {
                    // This is a send-and-receive operation. Modify the message info register to a
                    // value that doesn't affect the send behavior but, if interpreted in a receive
                    // context, would be invalid. Someone can then check after the syscall to
                    // detect this invalid state to determine if a bound notification was received.
                    // TODO: Pull the max number of caps out of the IDL instead of hard coding it
                    // here
                    quote!(
                        let mut info = info;

                        if info.get_extra_caps() < MSG_MAX_EXTRA_CAPS {
                            info.set_caps_unwrapped(1 << (MSG_MAX_EXTRA_CAPS - 1));
                        }
                        // If the number of sent caps == 3 then it's already invalid
                    )
                } else {
                    quote!()
                };

                let (asm_block, default_clobbers, clobbers_comma) = match arch {
                    Arch::X64 => (
                        quote!(
                            "movq %rsp, %rbx
                                   syscall
                                   movq %rbx, %rsp"
                        ),
                        quote!("rcx", "rbx", "r11"),
                        quote!(,),
                    ),
                    Arch::AArch64 => (quote!("svc #0"), quote!(), quote!()),
                };

                let memory_clobbers = if does_receive_ipcbuf {
                    quote!( #clobbers_comma "memory" )
                } else {
                    quote!()
                };

                write(
                    &mut out,
                    quote!(
                        impl #id_group {
                            #[inline(always)]
                            pub unsafe fn #syscall_name(&self, #input_params) #output_params {
                                #set_msginfo_sentinel
                                #self_arg_decl
                                #output_params_decl
                                llvm_asm!(
                                    #asm_block
                                    : #output_asm_block
                                    : #input_asm_block
                                    : #default_clobbers #memory_clobbers
                                    : "volatile"
                                );
                                #output_params_return
                            }
                        }
                    ),
                );
            }
            build_idl::Entry::Interface {
                name: iface_name,
                methods,
            } => {
                let struct_name = ident(&rustify_type(iface_name, word_size));
                let word_bits: u32 = match word_size {
                    Bits::Bits32 => 32,
                    Bits::Bits64 => 64,
                };
                let methods = methods.iter().map(|method| {
                    let mut method_name = ident(&rustify_name(method.name));
                    let vis = match overrides.get(&*format!("{}::{}", struct_name, method_name)) {
                        Some(name) if name.ends_with("_internal") => {
                            method_name = ident(name);
                            quote!( pub(crate) )
                        }
                        Some(name) => {
                            method_name = ident(name);
                            quote!(pub)
                        }
                        None => quote!(pub),
                    };
                    let invocation_label = method.invocation_label.as_u64();
                    let input_params = method.arguments.iter().map(|param| {
                        let name = ident(&rustify_name(param.name));
                        let type_ = ident(&rustify_type(param.type_, word_size));
                        quote!( #name: #type_, )
                    });
                    let marshal_input_caps = method
                        .arguments
                        .iter()
                        .filter(|param| param.is_cap)
                        .map(|param| {
                            let id_param_name = ident(&rustify_name(param.name));
                            if param.type_ == "seL4_CPtr" {
                                quote!( .push_cap(#id_param_name) )
                            } else {
                                quote!( .push_cap(#id_param_name.0.as_cptr()) )
                            }
                        });
                    let mut input_bit = 0;
                    let marshal_input_data = method
                        .arguments
                        .iter()
                        .filter(|param| !param.is_cap)
                        .flat_map(|param| {
                            // Iterate over and enumerate the fields inside of each argument
                            let info = type_info.get(param.type_).expect("Type information");

                            if let TypeInfo::Struct { fields } = info {
                                fields
                                    .iter()
                                    .map(|f| {
                                        (
                                            param,
                                            Some(f.name.clone()),
                                            f.type_.clone(),
                                            type_info
                                                .get::<str>(&f.type_)
                                                .expect("Type information"),
                                        )
                                    })
                                    .collect::<Vec<_>>()
                                    .into_iter()
                                    .enumerate()
                                } else {
                                    let mut v = Vec::new();
                                    v.push((param, None, param.type_.to_owned(), info));
                                    v.into_iter().enumerate()
                                }
                            })
                        .map(|(field_index, (param, opt_field_name, type_, info))| {
                            let id_param_name = ident(&rustify_name(param.name));
                            if field_index == 0 {
                                if let Some(_) = opt_field_name {
                                    // If this is the first field in a struct, round up bit position to
                                    // wordsize
                                    input_bit = align_up(input_bit, word_bits);
                                }
                            }
                            let (bits, field_accessor) = match info {
                                TypeInfo::Enum => (word_bits, quote!()),
                                TypeInfo::Type { bits } => if type_ == "seL4_Bool" {
                                    // Special case: booleans only use 1 bit
                                    (1, quote!())
                                } else {
                                    (*bits, quote!())
                                },
                                TypeInfo::Struct { .. } => panic!("Nested structs not supported in invocation {}::{} parameter {}", iface_name, method.name, param.name),
                                TypeInfo::BitfieldStruct { bits } => {
                                    (*bits, quote!(.0))
                                },
                            };

                            // TODO: We need to support this to compile 32-bit platforms
                            assert!(
                                bits <= word_bits,
                                "Values spanning multiple words are not supported in invocation {}::{} parameter {}",
                                iface_name, method.name, param.name,
                            );

                            // Parameters are naturally aligned
                            input_bit = align_up(input_bit, bits);

                            let begin = if input_bit % word_bits == 0 {
                                // if we're at the beginning of a word, close off the last push and
                                // start a new push
                                let lparen = Punct::new('(', Spacing::Alone);
                                let rparen = Punct::new(')', Spacing::Alone);
                                if input_bit > 0 {
                                    quote!(#rparen .push #lparen)
                                } else {
                                    quote!(.push #lparen)
                                }
                            } else {
                                // If we're in the middle of a word, we bitwise-or our value with
                                // the previous one
                                quote!(|)
                            };
                            let id_field = if let Some(field_name) = opt_field_name {
                                // This parameter is a structure and we're marshaling one of its
                                // fields
                                let id_field_name = ident(&rustify_name(&field_name));
                                quote!( .#id_field_name )
                            } else {
                                quote!()
                            };
                            let bit_pos = (input_bit % word_bits) as u8;
                            let (shift_lparens, shift, shift_rparens) = if bit_pos > 0 {
                                // If we're in the middle of a word, shift our value to the
                                // appropriate place
                                let lparen = Punct::new('(', Spacing::Alone);
                                let rparen = Punct::new(')', Spacing::Alone);
                                (quote!(#lparen), quote!(<< #bit_pos), quote!(#rparen))
                            } else {
                                (quote!(), quote!(), quote!())
                            };

                            input_bit += bits;

                            // Examples:
                            //   ) .push ( param as usize
                            //   ) .push ( param .field as usize
                            //   ) .push ( param .0 as usize
                            //   | ( param as usize ) << 8u8
                            quote!(#begin #shift_lparens #id_param_name #id_field #field_accessor as usize #shift_rparens #shift)
                        });
                    let input_ending_paren = if method.arguments.iter().filter(|p| !p.is_cap).count() > 0 {
                        let lparen = Punct::new(')', Spacing::Alone);
                        quote!(#lparen)
                    } else {
                        quote!()
                    };
                    let (msg_in_mut, msg_in_set_len) = if let Some(len) = get_interface_method_return_length_override(iface_name, method.name) {
                        (quote!(mut), quote!( msg_in.0.tag.set_length(#len); ))
                    } else {
                        (quote!(), quote!())
                    };

                    let mut output_params = TokenStream::new();
                    let mut output_params_decl = TokenStream::new();
                    let mut output_params_return = TokenStream::new();

                    for param in &method.return_values {
                        let info = type_info.get(param.type_).expect("Type information");
                        let id_type = ident(&rustify_type(param.type_, word_size));
                        let id_name = ident(&format!("out_{}", rustify_name(param.name)));
                        let output_quote = quote!( #id_type );
                        output_params_decl.extend(match info {
                            TypeInfo::Enum => quote!( let #id_name: usize; ),
                            TypeInfo::Type { bits: _ } => quote!( let #id_name: #id_type; ),
                            TypeInfo::Struct { .. } => quote!( let mut #id_name: #id_type = core::mem::zeroed(); ),
                            TypeInfo::BitfieldStruct { bits: _ } => quote!( let #id_name: usize; ),
                        });
                        let ret_quote = match info {
                            TypeInfo::Enum => quote!( core::mem::transmute(#id_name) ),
                            TypeInfo::Type { bits: _ } => quote!( #id_name ),
                            TypeInfo::Struct { .. } => quote!( #id_name ),
                            TypeInfo::BitfieldStruct { bits: _ } => quote!( #id_type.from_raw(#id_name) ),
                        };
                        if output_params.is_empty() {
                            output_params.extend(output_quote);
                            output_params_return.extend(ret_quote);
                        } else {
                            output_params.extend(quote!( , #output_quote ));
                            output_params_return.extend(quote!( , #ret_quote ));
                        }
                    }

                    output_params = match method.return_values.len() {
                        0 => quote!(()),
                        1 => quote!( #output_params ),
                        _ => quote!( (#output_params) ),
                    };

                    output_params_return = match method.return_values.len() {
                        0 => quote!(()),
                        1 => output_params_return,
                        _ => quote!( (#output_params_return) ),
                    };

                    let output_create_iter = if method.return_values.len() > 0 {
                        quote!( let mut args = msg_in.args(); )
                    } else {
                        quote!()
                    };

                    assert!(
                        method.return_values.iter().filter(|p| p.is_cap).count() == 0,
                        "Method invocation {}::{} receiving a capability in the reply message is not supported",
                        iface_name, method.name,
                    );

                    let mut output_bit = 0;
                    let marshal_output_data = method
                        .return_values
                        .iter()
                        .filter(|param| !param.is_cap)
                        .flat_map(|param| {
                            // Iterate over and enumerate the fields inside of each argument
                            let info = type_info.get(param.type_).expect("Type information");

                            if let TypeInfo::Struct { fields } = info {
                                fields
                                    .iter()
                                    .map(|f| {
                                        (
                                            param,
                                            Some(f.name.clone()),
                                            f.type_.clone(),
                                            type_info
                                                .get::<str>(&f.type_)
                                                .expect("Type information"),
                                        )
                                    })
                                    .collect::<Vec<_>>()
                                    .into_iter()
                                    .enumerate()
                                } else {
                                    let mut v = Vec::new();
                                    v.push((param, None, param.type_.to_owned(), info));
                                    v.into_iter().enumerate()
                                }
                            })
                        .map(|(field_index, (param, opt_field_name, type_, info))| {
                            let id_param_name = ident(&format!("out_{}", rustify_name(param.name)));
                            if field_index == 0 {
                                if let Some(_) = opt_field_name {
                                    // If this is the first field in a struct, round up bit position to
                                    // wordsize
                                    output_bit = align_up(output_bit, word_bits);
                                }
                            }
                            let bits = match info {
                                TypeInfo::Enum => word_bits,
                                TypeInfo::Type { bits } => if type_ == "seL4_Bool" {
                                    // Special case: booleans only use 1 bit
                                    1
                                } else {
                                    *bits
                                },
                                TypeInfo::Struct { .. } => panic!("Nested structs not supported in invocation {}::{} return value {}", iface_name, method.name, param.name),
                                TypeInfo::BitfieldStruct { bits } => *bits,
                            };

                            // TODO: We need to support this to compile 32-bit platforms
                            assert!(
                                bits <= word_bits,
                                "Values spanning multiple words are not supported in invocation {}::{} return value {}",
                                iface_name, method.name, param.name,
                            );

                            // Parameters are naturally aligned
                            output_bit = align_up(output_bit, bits);

                            let begin = if output_bit % word_bits == 0 {
                                // if we're at the beginning of a word, move on to the next
                                // value in the argument iterator
                                quote!( let arg = args.next().unwrap(); )
                            } else {
                                quote!()
                            };
                            let id_field = if let Some(field_name) = opt_field_name {
                                // This parameter is a structure and we're marshaling one of its
                                // fields
                                let id_field_name = ident(&rustify_name(&field_name));
                                quote!( .#id_field_name )
                            } else {
                                quote!()
                            };
                            let bit_pos = (output_bit % word_bits) as u8;
                            let (shift_lparens, shift, shift_rparens) = if bit_pos > 0 {
                                // If we're in the middle of a word shift over so our data starts
                                // at bit 0
                                let lparen = Punct::new('(', Spacing::Alone);
                                let rparen = Punct::new(')', Spacing::Alone);
                                (quote!(#lparen), quote!(>> #bit_pos), quote!(#rparen))
                            } else {
                                (quote!(), quote!(), quote!())
                            };
                            let (mask_lparens, mask, mask_rparens) = if bits == word_bits {
                                (quote!(), quote!(), quote!())
                            } else {
                                // if we're not reading a word-sized bit, mask off the bits that
                                // aren't for us
                                let lparen = Punct::new('(', Spacing::Alone);
                                let rparen = Punct::new(')', Spacing::Alone);
                                let bitmask = {
                                    let mut ret = 0u64;
                                    for i in 0..bits {
                                        ret |= 1 << i;
                                    }
                                    ret
                                };
                                (quote!(#lparen), quote!(& #bitmask as usize), quote!(#rparen))
                            };

                            output_bit += bits;

                            // Examples:
                            // let arg = args.next().unwrap(); out_value = arg as _;
                            // out_value = ((arg >> 8) & 255u64 as usize) as _;
                            quote!( #begin #id_param_name #id_field = #mask_lparens #shift_lparens arg #shift #shift_rparens #mask #mask_rparens as _; )
                        });

                    quote!(
                        #[inline(always)]
                        #vis unsafe fn #method_name(&self, #( #input_params )* ipcbuf: &mut IPCBuffer )
                        -> Result<#output_params, Error>
                        {
                            let #msg_in_mut msg_in = self.0.call(
                                ipcbuf
                                    .new_message()
                                    .label(#invocation_label as usize)
                                    #( #marshal_input_caps )*
                                    #( #marshal_input_data )* #input_ending_paren
                            );

                            if msg_in.label() == 0 {
                                // If this is a bugged invocation that doesn't return the correct
                                // length, set it here
                                #msg_in_set_len
                                // TODO: Once we have our own error type, return an error if the
                                // length isn't the expected length
                                #output_params_decl
                                #output_create_iter
                                #( #marshal_output_data )*
                                Ok(#output_params_return)
                            } else {
                                Err(core::mem::transmute(msg_in.label()))
                            }
                        }
                    )
                });

                write(
                    &mut out,
                    quote!(
                        impl #struct_name {
                            #( #methods )*
                        }
                    ),
                );
            }
            build_idl::Entry::Error { name, type_ } => {
                write(
                    &mut out,
                    format!(
                        "\n\n// TODO FIXME BUGBUG: IDL File is missing a definition for {}: {}\n\n",
                        type_, name,
                    ),
                );
            }
        }
    }

    //
    // Write files
    //

    // flush and close the file as we're about to ask rustfmt to reformat it.
    drop(out);

    // update the runner environment so it can find the kernel
    fs::write(
        cargo_manifest.join(".cargo").join("env"),
        format!("OUT_DIR={}", outdir.display()),
    )
    .expect("failed to write environment information for the runner");

    if let Err(_) = process::Command::new("rustfmt")
        .arg("generated.rs")
        .current_dir(outdir)
        .output()
    {
        println!(r#"cargo:warning="rustfmt is not installed. could not format generated.rs.""#);
    }
}

fn write(out: &mut impl Write, tokens: impl core::fmt::Display) {
    write!(out, "{}", tokens).unwrap();
}

fn ident(s: &str) -> Ident {
    Ident::new(s, Span::call_site())
}

fn rustify_type(s: &str, word_size: Bits) -> String {
    if let Some(t) = get_type_override(s) {
        return t.to_owned();
    }

    let (u_long, s_long) = match word_size {
        Bits::Bits32 => ("u32", "i32"),
        Bits::Bits64 => ("u64", "i64"),
    };

    match s {
        "char" => return "i8".to_owned(),
        "signed char" => return "i8".to_owned(),
        "unsigned char" => return "u8".to_owned(),
        "signed short" => return "i16".to_owned(),
        "unsigned short" => return "u16".to_owned(),
        "signed int" => return "i32".to_owned(),
        "unsigned int" => return "u32".to_owned(),
        "signed long" => return s_long.to_owned(),
        "unsigned long" => return u_long.to_owned(),
        "signed long long" => return "i64".to_owned(),
        "unsigned long long" => return "u64".to_owned(),
        _ => {}
    };

    let s = s
        .trim_start_matches("seL4_")
        .trim_start_matches("SEL4_")
        .trim_end_matches("_t")
        .to_pascal_case();

    match &*s {
        // Correct case for all UPPERCASE things
        "Tcb" => "TCB",
        // x64
        "X64PmL4" => "X64PML4",
        "X64Pdpt" => "X64PDPT",
        // x86
        "X86Eptpd" => "X86EPTPD",
        "X86Eptpt" => "X86EPTPT",
        "X86Eptpdpt" => "X86EPTPDPT",
        "X86EptpmL4" => "X86EPTPML4",
        "X86Vcpu" => "X86VCPU",
        // Manual name overrides
        "IPCBuffer" => "RawIPCBuffer",
        _ => &s,
    }
    .to_string()
}

fn get_type_override(t: &str) -> Option<&'static str> {
    for (c_type, rust_type) in TYPE_OVERRIDES {
        if c_type == &t {
            return Some(rust_type);
        }
    }

    None
}

fn get_syscall_group(s: &str) -> SyscallGroup {
    for (syscall, syscall_group) in SYSCALL_GROUPS {
        if &s == syscall {
            return *syscall_group;
        }
    }

    panic!(
        "No syscall group defined for syscall '{}', please add it to build.rs",
        s
    );
}

fn rustify_name(s: &str) -> String {
    let s = s
        .trim_start_matches("seL4_")
        .trim_start_matches("SEL4_")
        .to_snake_case();
    let s = match &*s {
        // HACKS: use a complete list of rust keywords
        "move" => "move_",
        "type" => "type_",
        "yield" => "yield_",
        _ => &s,
    };

    let s = if s.chars().nth(0).unwrap().is_digit(10) {
        let mut tmp = "_".to_string();
        tmp.push_str(s);
        tmp
    } else {
        s.to_string()
    };

    // correct some incorrect case conversions
    let s = s
        .replace("pm_l_4", "pml4_")
        .replace("_c_r3", "_cr3")
        .replace("i_a32_", "ia32_")
        .replace("v_space_", "vspace_")
        .replace("_c_node", "_cnode");

    if s.starts_with("m_r") {
        s.replacen("m_r", "mr", 1)
    } else {
        s
    }
}

fn align_up(value: u32, size: u32) -> u32 {
    if value % size == 0 {
        value
    } else {
        value + size - (value % size)
    }
}

// Detect whether this method is bugged and the kernel does not correctly set the message info
// register. If so, return the correct length.
fn get_interface_method_return_length_override(iface: &str, method: &str) -> Option<usize> {
    match (iface, method) {
        ("seL4_X86_Page", "GetAddress") => Some(1),
        ("seL4_ARM_Page", "GetAddress") => Some(1),
        ("seL4_RISCV_Page", "GetAddress") => Some(1),
        ("seL4_X86_VCPU", "ReadVMCS") => Some(1),
        ("seL4_SchedContext", "Consumed") => Some(1),
        ("seL4_SchedContext", "YieldTo") => Some(1),
        _ => None,
    }
}
