use core::marker;

use embedded_hal::serial;

// this file is not allowed to reference sel4!

pub trait IOPort<Word> {
    type Error;

    fn read(&mut self, port: u16) -> nb::Result<Word, Self::Error>;
    fn write(&mut self, port: u16, word: Word) -> nb::Result<(), Self::Error>;
}

pub trait IRQ {
    type Error;

    fn poll(&mut self) -> nb::Result<(), Self::Error>;
    fn acknowledge(&mut self) -> nb::Result<(), Self::Error>;
}

// TODO: PCI/IOMMU & MMIO

pub mod devices {
    use super::*;

    pub struct Serial<IOP, IRQH>
    where
        IOP: IOPort<u8>,
        IRQH: IRQ,
    {
        ioport: IOP,
        irq: IRQH,
    }

    impl<IOP, IRQH, E> Serial<IOP, IRQH>
    where
        IOP: IOPort<u8, Error = E>,
        IRQH: IRQ<Error = E>,
        E: core::fmt::Debug,
    {
        fn new(mut ioport: IOP, irq: IRQH) -> Self {
            // enable interrupts on our serial port
            ioport.write(1, 0x01).unwrap();
            Serial { ioport, irq }
        }
    }

    impl<IOP, IRQH, E> serial::Read<u8> for Serial<IOP, IRQH>
    where
        IOP: IOPort<u8, Error = E>,
        IRQH: IRQ<Error = E>,
    {
        type Error = E;

        fn read(&mut self) -> nb::Result<u8, Self::Error> {
            loop {
                if self.ioport.read(5)? & 0x01 == 0x01 {
                    break self.ioport.read(0);
                } else {
                    self.irq.acknowledge()?;
                    if let Err(e) = self.irq.poll() {
                        break Err(e);
                    }
                }
            }
        }
    }

    // does the writer need a different interrupt/notificaiton pair?
    impl<IOP, IRQH, E> serial::Write<u8> for Serial<IOP, IRQH>
    where
        IOP: IOPort<u8, Error = E>,
        IRQH: IRQ<Error = E>,
    {
        type Error = E;

        fn write(&mut self, word: u8) -> nb::Result<(), Self::Error> {
            loop {
                if self.ioport.read(5)? & 0x20 == 0x20 {
                    break self.ioport.write(0, word);
                } else {
                    self.irq.acknowledge()?;
                    if let Err(e) = self.irq.poll() {
                        break Err(e);
                    }
                }
            }
        }

        fn flush(&mut self) -> nb::Result<(), Self::Error> {
            Ok(())
        }
    }
}
