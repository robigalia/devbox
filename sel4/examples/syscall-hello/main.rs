#![feature(core_intrinsics, lang_items)]
#![no_std]

#[cfg(not(feature = "CONFIG_DEBUG_BUILD"))]
compile_error!("This example requires KernelDebugBuild to be turned on in your seL4 config");

#[cfg(not(feature = "CONFIG_PRINTING"))]
compile_error!("This example requires KernelPrinting to be turned on in your seL4 config");

use core::fmt::Write;

mod start;

fn main() {
    let (_, _) = start::get_boot_info();
    let caps = sel4::bootinfo::take_root_caps();
    let debug = caps.debug;
    let mut debug_printer = caps.debug_printer;

    let _ = writeln!(debug_printer, "");
    let _ = writeln!(debug_printer, "Hello world!");
    let _ = writeln!(debug_printer, "");
    let _ = writeln!(debug_printer, "To exit qemu, press: <control>+<a> <x>.");
    let _ = writeln!(debug_printer, "");

    unsafe { debug.halt() };
}
