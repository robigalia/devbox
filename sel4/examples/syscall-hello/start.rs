use core::{intrinsics, panic, sync};

static mut BOOTINFO: *const sel4::BootInfo = core::ptr::null();

#[panic_handler]
fn panic_handler(_: &panic::PanicInfo) -> ! {
    intrinsics::abort()
}

#[lang = "eh_personality"]
fn eh_personality() -> ! {
    intrinsics::abort()
}

#[lang = "oom"]
fn oom(_layout: core::alloc::Layout) -> ! {
    intrinsics::abort()
}

#[lang = "termination"]
trait Termination {
    fn report(self) -> i32;
}

impl Termination for () {
    fn report(self) -> i32 {
        0
    }
}

#[lang = "start"]
fn lang_start<T: Termination>(main: fn() -> T, _argc: isize, _argv: *const *const u8) -> isize {
    main();

    intrinsics::abort();
}

#[no_mangle]
unsafe extern "C" fn __sel4_start_init_boot_info(bootinfo: *const sel4::BootInfo) {
    // stash away the bootinfo
    BOOTINFO = bootinfo;
}

pub fn get_boot_info() -> (&'static sel4::BootInfo, sel4::IPCBuffer) {
    use sync::atomic::{AtomicBool, Ordering};

    static RUN_ONCE: AtomicBool = AtomicBool::new(false);

    if !RUN_ONCE.swap(true, Ordering::Relaxed) {
        unsafe {
            (
                BOOTINFO
                    .as_ref()
                    .expect("BootInfo was not set by the boot stub"),
                sel4::IPCBuffer::from_raw((*BOOTINFO).ipc_buffer),
            )
        }
    } else {
        panic!("get_boot_info was called more than once, it can only be called once");
    }
}

#[cfg(feature = "CONFIG_ARCH_X86_64")]
core::arch::global_asm!(include_str!("start_x64.S"), options(att_syntax, raw));

#[cfg(feature = "CONFIG_ARCH_AARCH64")]
core::arch::global_asm!(include_str!("start_aarch64.S"), options(raw));
