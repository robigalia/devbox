use crate::{CPtr, MessageInfo, RawIPCBuffer, FAST_MESSAGE_REGISTERS};

/// This structure owns the thread-local IPC buffer.
///
/// It cannot be cloned or sent to another thread.
///
/// It is currently implemented by keeping a pointer to the IPC buffer; however, the interface is
/// intended to work for the case where the IPC buffer is accessed indirectly through a cpu
/// register. Implementing that case is left for future work.
///
/// All functions in this library that access the IPC Buffer require a reference to this structure
/// from the caller (either directly or indirectly as a member in another structure).
///
/// This allows the rust type system to guarantee at compile time:
///
///   - You can't receive into the IPC buffer if there exists a reference into the IPC Buffer
///   - You can't send from the IPC buffer if there exists a mutable reference into the IPC Buffer
///
#[repr(transparent)]
pub struct IPCBuffer(pub(crate) *mut RawIPCBuffer);

impl !Send for IPCBuffer {}

impl !Sync for IPCBuffer {}

impl IPCBuffer {
    #[inline(always)]
    pub unsafe fn from_raw(raw: *mut RawIPCBuffer) -> Self {
        Self(raw)
    }

    #[inline(always)]
    pub fn into_raw(self) -> *mut RawIPCBuffer {
        self.0 as _
    }

    #[inline(always)]
    pub fn new_message<'a>(&'a mut self) -> IPCMessageOut {
        IPCMessageOut::new(self)
    }

    // Unsafe accessor functions into the ipc buffer are added in build.rs
}

/// Constructs a message in the IPC Buffer using the owned builder pattern.
///
/// This structure holds a mutable reference to the IPCBuffer. It writes to the IPCBuffer only if
/// you send more than the architecture-defined number of words passed in cpu registers or send
/// capabilities.
///
/// Use the new_message method on the IPCBuffer to create this structure.
///
pub struct IPCMessageOut<'ipcbuf> {
    pub(crate) ipcbuf: &'ipcbuf mut IPCBuffer,
    pub(crate) tag: MessageInfo,
    pub(crate) cpureg: [usize; FAST_MESSAGE_REGISTERS],
}

impl !Send for IPCMessageOut<'_> {}

impl !Sync for IPCMessageOut<'_> {}

impl<'ipcbuf> IPCMessageOut<'ipcbuf> {
    #[inline(always)]
    fn new<'b>(ipcbuf: &'ipcbuf mut IPCBuffer) -> IPCMessageOut<'ipcbuf> {
        let tag = MessageInfo::new().build();
        let cpureg = [0; FAST_MESSAGE_REGISTERS];

        Self {
            ipcbuf,
            tag,
            cpureg,
        }
    }

    #[inline(always)]
    pub fn label(mut self, item: usize) -> Self {
        self.tag.set_label(item);
        self
    }

    #[inline(always)]
    pub fn push(mut self, item: usize) -> Self {
        let i = self.tag.get_length();
        if i < FAST_MESSAGE_REGISTERS {
            self.cpureg[i] = item;
        } else {
            unsafe {
                self.ipcbuf.get_msg_mut()[i] = item;
            }
        }

        self.tag.set_length(i + 1);
        self
    }

    #[inline(always)]
    pub fn push_slice(mut self, msg: &[usize]) -> Self {
        let mut i = self.tag.get_length();
        for arg in msg.into_iter() {
            if i < FAST_MESSAGE_REGISTERS {
                self.cpureg[i] = *arg;
            } else {
                unsafe {
                    self.ipcbuf.get_msg_mut()[i] = *arg;
                }
            }
            i += 1;
        }
        self.tag.set_length(i);
        self
    }

    #[inline(always)]
    pub fn push_cap(mut self, c: CPtr) -> Self {
        let i = self.tag.get_extra_caps();
        unsafe { self.ipcbuf.get_caps_or_badges_mut()[i] = c.as_usize() };
        self.tag.set_extra_caps(i + 1);
        self
    }
}

/// Helper structure to read a received IPC message.
///
/// This structure is returned by syscalls that receive messages.
///
/// This structure holds a reference to the IPCBuffer.
///
#[must_use]
pub struct IPCMessageIn<'ipcbuf> {
    ipcbuf: &'ipcbuf IPCBuffer,
    pub(crate) tag: MessageInfo,
    cpureg: [usize; FAST_MESSAGE_REGISTERS],
    badge: usize,
}

impl !Send for IPCMessageIn<'_> {}

impl !Sync for IPCMessageIn<'_> {}

impl<'ipcbuf> IPCMessageIn<'ipcbuf> {
    #[inline(always)]
    pub(crate) fn new(
        ipcbuf: &'ipcbuf IPCBuffer,
        badge: usize,
        tag: MessageInfo,
        cpureg: [usize; FAST_MESSAGE_REGISTERS],
    ) -> IPCMessageIn {
        Self {
            ipcbuf,
            tag,
            cpureg,
            badge,
        }
    }

    #[inline(always)]
    pub fn badge(&self) -> usize {
        self.badge
    }

    #[inline(always)]
    pub fn label(&self) -> usize {
        self.tag.get_label()
    }

    #[inline(always)]
    pub fn args(&self) -> IPCMessageArgs {
        IPCMessageArgs::new(self)
    }

    #[inline(always)]
    pub fn caps(&self) -> IPCMessageCaps {
        IPCMessageCaps::new(self)
    }
}

/// A message received as a reply to a call operation.
///
/// This has the same properties as an IPCMessageIn except without the badge, which is meaningless
/// in this context.
#[must_use]
pub struct IPCMessageInCallReply<'ipcbuf>(pub(crate) IPCMessageIn<'ipcbuf>);

impl !Send for IPCMessageInCallReply<'_> {}

impl !Sync for IPCMessageInCallReply<'_> {}

impl<'ipcbuf> IPCMessageInCallReply<'ipcbuf> {
    #[inline(always)]
    pub fn label(&self) -> usize {
        self.0.label()
    }

    #[inline(always)]
    pub fn args(&self) -> IPCMessageArgs {
        self.0.args()
    }

    #[inline(always)]
    pub fn caps(&self) -> IPCMessageCaps {
        self.0.caps()
    }
}

/// Iterator over word-sized arguments received in an IPCMessageIn.
///
/// You can create this structure by calling the args method on an IPCMessageIn struct.
///
/// This structure holds a reference to the IPCMessageIn and, indirectly, the IPCBuffer.
pub struct IPCMessageArgs<'msg, 'ipcbuf> {
    msg: &'msg IPCMessageIn<'ipcbuf>,
    pos: usize,
}

impl !Send for IPCMessageArgs<'_, '_> {}

impl !Sync for IPCMessageArgs<'_, '_> {}

impl<'msg, 'ipcbuf> IPCMessageArgs<'msg, 'ipcbuf> {
    #[inline(always)]
    fn new(msg: &'msg IPCMessageIn<'ipcbuf>) -> Self {
        Self { msg, pos: 0 }
    }

    #[inline(always)]
    fn get(&self, i: usize) -> Option<usize> {
        if i < FAST_MESSAGE_REGISTERS {
            Some(self.msg.cpureg[i])
        } else {
            unsafe { Some(self.msg.ipcbuf.get_msg_ref()[i]) }
        }
    }
}

impl Iterator for IPCMessageArgs<'_, '_> {
    type Item = usize;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let i = self.pos;
        if i < self.msg.tag.get_length() {
            self.pos += 1;
            self.get(i)
        } else {
            None
        }
    }

    #[inline(always)]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.msg.tag.get_length() - self.pos;
        (size, Some(size))
    }
}

impl ExactSizeIterator for IPCMessageArgs<'_, '_> {
    #[inline(always)]
    fn len(&self) -> usize {
        self.msg.tag.get_length() - self.pos
    }
}

/// Iterator over capabilities received in an IPCMessageIn.
///
/// The received capabilities are either received as an invokable capability into the CNode
/// location specified in the IPC buffer or are unwrapped into their badge value if they were
/// derived from the endpoint that they were received on.
///
/// You can create this structure by calling the caps method on an IPCMessageIn struct.
///
/// This structure holds a reference to the IPCMessageIn and, indirectly, the IPCBuffer.
pub struct IPCMessageCaps<'msg, 'ipcbuf> {
    msg: &'msg IPCMessageIn<'ipcbuf>,
    pos: usize,
}

impl !Send for IPCMessageCaps<'_, '_> {}

impl !Sync for IPCMessageCaps<'_, '_> {}

impl<'msg, 'ipcbuf> IPCMessageCaps<'msg, 'ipcbuf> {
    #[inline(always)]
    fn new(msg: &'msg IPCMessageIn<'ipcbuf>) -> Self {
        Self { msg, pos: 0 }
    }

    #[inline(always)]
    fn get(&self, i: usize) -> Option<IPCMessageCap> {
        let unwrapped_flags = self.msg.tag.get_caps_unwrapped();
        let bit = 1 << i;
        if unwrapped_flags & bit == bit {
            Some(IPCMessageCap::Unwrapped(unsafe {
                self.msg.ipcbuf.get_caps_or_badges_ref()[i]
            }))
        } else {
            unsafe {
                Some(IPCMessageCap::Cap {
                    cnode: self.msg.ipcbuf.get_receive_cnode(),
                    index: self.msg.ipcbuf.get_receive_index(),
                    depth: self.msg.ipcbuf.get_receive_depth(),
                })
            }
        }
    }
}

impl Iterator for IPCMessageCaps<'_, '_> {
    type Item = IPCMessageCap;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let i = self.pos;
        if i < self.msg.tag.get_extra_caps() {
            self.pos += 1;
            self.get(i)
        } else {
            None
        }
    }

    #[inline(always)]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.msg.tag.get_extra_caps() - self.pos;
        (size, Some(size))
    }
}

impl ExactSizeIterator for IPCMessageCaps<'_, '_> {
    #[inline(always)]
    fn len(&self) -> usize {
        self.msg.tag.get_extra_caps() - self.pos
    }
}

/// A capability received from an IPC message.
///
/// The capability was either received into the CNode specified in the IPC buffer (included here
/// for reference) or it was unwrapped into its badge value because it was derived from the endpoint
/// that it was received on.
pub enum IPCMessageCap {
    Cap {
        cnode: CPtr,
        index: usize,
        depth: usize,
    },
    Unwrapped(usize),
}
