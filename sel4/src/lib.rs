#![feature(
    asm,
    const_fn,
    doc_cfg,
    crate_visibility_modifier,
    const_raw_ptr_to_usize_cast,
    const_raw_ptr_deref,
    llvm_asm,
    negative_impls
)]
#![no_std]
#![cfg_attr(feature = "cargo-clippy", warn(unsafe_code))]

#[cfg(not(target_os = "sel4"))]
compile_error!("target_os /= sel4. Did you forget --target x86_64-sel4-robigalia?");

#[allow(
    clippy::needless_pass_by_value,
    clippy::unused_unit,
    clippy::too_many_arguments
)]
mod generated {
    use crate::*;

    include!(concat!(env!("OUT_DIR"), "/generated.rs"));
}

mod benchmark;
mod cap;
mod debug;
mod endpoint;
mod ipcbuffer;
mod notification;
mod virtualization;
pub use crate::benchmark::Benchmark;
pub use crate::cap::{CPtr, Cap};
pub use crate::debug::{Debug, DebugPrinter};
pub use crate::endpoint::{Received, ReplyToken};
pub use crate::generated::*;
pub use crate::ipcbuffer::{
    IPCBuffer, IPCMessageArgs, IPCMessageCap, IPCMessageCaps, IPCMessageIn, IPCMessageInCallReply,
    IPCMessageOut,
};
pub use crate::virtualization::Virtualization;

macro_rules! cap_wrapper {
    ($($tt:tt),* $(,)*) => {
        $(
            pub struct $tt(Cap);

            impl AsRef<Cap> for $tt {
                fn as_ref(&self) -> &Cap {
                    &self.0
                }
            }

            impl From<Cap> for $tt {
                fn from(cap: Cap) -> Self {
                    $tt(cap)
                }
            }
         )*
    }
}

cap_wrapper!(Notification, Endpoint);

#[cfg(feature = "CONFIG_KERNEL_MCS")]
cap_wrapper!(Reply);

#[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
/// Represents the slot in the TCB that holds a single reply capability.
///
/// You receive a single instance of this struct from bootinfo::take_boot_info().
///
/// Cannot be sent to another thread.
///
/// IPC receive methods on an Endpoint or ReplyToken will return ReplyTokens that reference this
/// object. The methods on the ReplyToken should be used to send replies.
pub struct Reply(());

#[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
impl !Send for Reply {}

#[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
impl !Sync for Reply {}

// Home of the yield syscall
pub struct Yield(());

// RENAME: IRQHandler::ack = acknowledge
// RENAME: X86IOPort::in8 = read8
// RENAME: X86IOPort::in16 = read16
// RENAME: X86IOPort::in32 = read32
// RENAME: X86IOPort::out8 = write8
// RENAME: X86IOPort::out16 = write16
// RENAME: X86IOPort::out32 = write32

impl TCB {
    // OVERRIDE: TCB::read_registers
    // Enforce all registers are copied
    pub unsafe fn read_registers(
        &self,
        suspend_source: bool,
        arch_flags: u8,
        ipcbuf: &mut IPCBuffer,
    ) -> Result<UserContext, Error> {
        self.read_registers_internal(
            suspend_source,
            arch_flags,
            core::mem::size_of::<UserContext>() / core::mem::size_of::<usize>(),
            ipcbuf,
        )
    }

    // OVERRIDE: TCB::write_registers
    // Enforce all registers are copied
    pub unsafe fn write_registers(
        &self,
        suspend_source: bool,
        arch_flags: u8,
        user_context: UserContext,
        ipcbuf: &mut IPCBuffer,
    ) -> Result<(), Error> {
        self.write_registers_internal(
            suspend_source,
            arch_flags,
            core::mem::size_of::<UserContext>() / core::mem::size_of::<usize>(),
            user_context,
            ipcbuf,
        )
    }

    // OVERRIDE-: TCB::get_breakpoint
    // OVERRIDE-: TCB::set_breakpoint
}

#[cfg(feature = "CONFIG_KERNEL_MCS")]
impl SchedContext {
    // OVERRIDE: SchedContext::bind
    pub unsafe fn bind<C: AsRef<Cap>>(
        &self,
        // WEAK: should only accept TCB or Notification
        cap: C,
        ipcbuf: &mut IPCBuffer,
    ) -> Result<(), Error> {
        self.bind_internal(cap.as_ref().as_cptr(), ipcbuf)
    }

    // OVERRIDE: SchedContext::unbind_object
    pub unsafe fn unbind_object<C: AsRef<Cap>>(
        self,
        // WEAK: should only accept TCB or Notification
        cap: C,
        ipcbuf: &mut IPCBuffer,
    ) -> Result<(), Error> {
        self.unbind_object_internal(cap.as_ref().as_cptr(), ipcbuf)
    }
}

#[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
impl CNode {
    // OVERRIDE: CNode::save_caller
    // This wrapper requires a ReplyToken to guarantee at compile time that it can only be called when
    // a reply capability should exist in the TCB. Furthermore, it consumes the ReplyToken so the Reply
    // can no longer be executed, as this moves the capability from the TCB into the destination slot.
    pub unsafe fn save_caller(
        &self,
        _reply_token: ReplyToken,
        index: usize,
        depth: u8,
        ipcbuf: &mut IPCBuffer,
    ) -> Result<(), Error> {
        self.save_caller_internal(index, depth, ipcbuf)
    }
}

// OVERRIDE-: CNode??
// https://gitlab.com/robigalia/rust-sel4/blob/master/src/cspace.rs
// Use SlotRef to wrap (root, guard, depth)

#[cfg(any(feature = "root-task", rustdoc))]
pub mod bootinfo {
    use crate::*;

    // TODO: This should be defined in the IDL
    #[cfg(feature = "CONFIG_ARCH_X86_64")]
    pub struct RootCaps {
        pub null: Cap,
        pub init_thread_tcb: TCB,
        pub init_thread_cnode: CNode,
        pub init_thread_vspace: X64PML4,
        pub irq_control: IRQControl,
        pub asid_control: X86ASIDControl,
        pub init_thread_asid_pool: X86ASIDPool,
        pub ioport_control: X86IOPortControl,
        pub iospace: X86IOSpace,
        pub boot_info_frame: X86Page,
        pub init_thread_ipc_buffer: X86Page,
        pub domain: DomainSet,
        #[cfg(feature = "CONFIG_KERNEL_MCS")]
        pub init_thread_sched_context: SchedContext,
        pub num_initial_caps: usize,
        pub benchmark: benchmark::Benchmark,
        pub debug: debug::Debug,
        pub debug_printer: debug::DebugPrinter,
        pub virtualization: virtualization::Virtualization,
        pub yield_: Yield,
        #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
        pub reply: Reply,
        //  pub bootinfo: &'static mut Bootinfo,
    }

    #[cfg(feature = "CONFIG_ARCH_AARCH64")]
    pub struct RootCaps {
        pub null: Cap,
        pub init_thread_tcb: TCB,
        pub init_thread_cnode: CNode,
        pub init_thread_vspace: ARMPageGlobalDirectory,
        pub irq_control: IRQControl,
        pub asid_control: ARMASIDControl,
        pub init_thread_asid_pool: ARMASIDPool,
        pub ioport_control: Cap,
        pub iospace: ARMIOSpace,
        pub boot_info_frame: ARMPage,
        pub init_thread_ipc_buffer: ARMPage,
        pub domain: DomainSet,
        #[cfg(feature = "CONFIG_KERNEL_MCS")]
        pub init_thread_sched_context: SchedContext,
        pub num_initial_caps: usize,
        pub benchmark: benchmark::Benchmark,
        pub debug: debug::Debug,
        pub debug_printer: debug::DebugPrinter,
        pub virtualization: virtualization::Virtualization,
        pub yield_: Yield,
        #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
        pub reply: Reply,
        //  pub bootinfo: &'static mut Bootinfo,
    }

    use core::sync::atomic::{AtomicBool, Ordering};

    static TAKEN_ROOT_CAPS: AtomicBool = AtomicBool::new(false);

    #[cfg(feature = "internal-untake-root-caps")]
    pub unsafe fn untake_root_caps(_: RootCaps) {
        assert!(
            TAKEN_ROOT_CAPS.swap(false, Ordering::Relaxed),
            "Root caps had not yet been taken!",
        );
    }

    pub fn take_root_caps() -> RootCaps {
        assert!(
            !TAKEN_ROOT_CAPS.swap(true, Ordering::Relaxed),
            "Root caps have already been taken!",
        );

        #[cfg(feature = "CONFIG_ARCH_X86_64")]
        unsafe {
            RootCaps {
                null: Cap::from_cptr(CPtr(0)),
                init_thread_tcb: TCB::from(Cap::from_cptr(CPtr(1))),
                init_thread_cnode: CNode::from(Cap::from_cptr(CPtr(2))),
                init_thread_vspace: X64PML4::from(Cap::from_cptr(CPtr(3))),
                irq_control: IRQControl::from(Cap::from_cptr(CPtr(4))),
                asid_control: X86ASIDControl::from(Cap::from_cptr(CPtr(5))),
                init_thread_asid_pool: X86ASIDPool::from(Cap::from_cptr(CPtr(6))),
                ioport_control: X86IOPortControl::from(Cap::from_cptr(CPtr(7))),
                iospace: X86IOSpace::from(Cap::from_cptr(CPtr(8))),
                boot_info_frame: X86Page::from(Cap::from_cptr(CPtr(9))),
                init_thread_ipc_buffer: X86Page::from(Cap::from_cptr(CPtr(10))),
                domain: DomainSet::from(Cap::from_cptr(CPtr(11))),
                #[cfg(feature = "CONFIG_KERNEL_MCS")]
                init_thread_sched_context: SchedContext::from(Cap::from_cptr(CPtr(12))),
                #[cfg(feature = "CONFIG_KERNEL_MCS")]
                num_initial_caps: 13,
                #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
                num_initial_caps: 12,
                benchmark: benchmark::Benchmark(()),
                debug: debug::Debug(()),
                debug_printer: debug::DebugPrinter(()),
                virtualization: virtualization::Virtualization(()),
                yield_: Yield(()),
                #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
                reply: Reply(()),
            }
        }

        #[cfg(feature = "CONFIG_ARCH_AARCH64")]
        unsafe {
            RootCaps {
                null: Cap::from_cptr(CPtr(0)),
                init_thread_tcb: TCB::from(Cap::from_cptr(CPtr(1))),
                init_thread_cnode: CNode::from(Cap::from_cptr(CPtr(2))),
                init_thread_vspace: ARMPageGlobalDirectory::from(Cap::from_cptr(CPtr(3))),
                irq_control: IRQControl::from(Cap::from_cptr(CPtr(4))),
                asid_control: ARMASIDControl::from(Cap::from_cptr(CPtr(5))),
                init_thread_asid_pool: ARMASIDPool::from(Cap::from_cptr(CPtr(6))),
                ioport_control: Cap::from_cptr(CPtr(7)),
                iospace: ARMIOSpace::from(Cap::from_cptr(CPtr(8))),
                boot_info_frame: ARMPage::from(Cap::from_cptr(CPtr(9))),
                init_thread_ipc_buffer: ARMPage::from(Cap::from_cptr(CPtr(10))),
                domain: DomainSet::from(Cap::from_cptr(CPtr(11))),
                #[cfg(feature = "CONFIG_KERNEL_MCS")]
                init_thread_sched_context: SchedContext::from(Cap::from_cptr(CPtr(12))),
                #[cfg(feature = "CONFIG_KERNEL_MCS")]
                num_initial_caps: 13,
                #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
                num_initial_caps: 12,
                benchmark: benchmark::Benchmark(()),
                debug: debug::Debug(()),
                debug_printer: debug::DebugPrinter(()),
                virtualization: virtualization::Virtualization(()),
                yield_: Yield(()),
                #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
                reply: Reply(()),
            }
        }
    }
}
