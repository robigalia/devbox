use crate::*;

impl Notification {
    pub fn signal(&self, ipcbuf: &mut IPCBuffer) -> Result<usize, Error> {
        // This is kind of a hack. Signal on a notification could return an error code in the
        // label; however, the kernel does not write to the label on success. We work around this
        // by setting our label to 0 in the call. If the kernel does not modify the label to an
        // error code, it will retain the 0 value from our call, and we know it was successful.
        // See https://github.com/seL4/seL4/issues/114
        let msg_in = unsafe { self.0.call(ipcbuf.new_message()) };
        let label = msg_in.label();

        if label == 0 {
            Ok(msg_in.0.badge())
        } else {
            unsafe { Err(core::mem::transmute(label)) }
        }
    }

    // While poll and wait are conceptually receive operations, seL4 does not fill out the message
    // registers the same way that a receive against an endpoint would. It is only safe to access
    // the badge property below.

    #[cfg(feature = "CONFIG_KERNEL_MCS")]
    pub fn poll(&self, ipcbuf: &mut IPCBuffer) -> Option<usize> {
        let msg = unsafe { self.0.nb_wait(ipcbuf) };
        let badge = msg.badge();

        if badge != 0 {
            Some(badge)
        } else {
            None
        }
    }

    #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
    pub fn poll(&self, ipcbuf: &mut IPCBuffer) -> Option<usize> {
        let msg = unsafe { self.0.nb_recv(ipcbuf) };
        let badge = msg.badge();

        if badge != 0 {
            Some(badge)
        } else {
            None
        }
    }

    #[cfg(feature = "CONFIG_KERNEL_MCS")]
    pub fn wait(&self, ipcbuf: &mut IPCBuffer) -> usize {
        unsafe { self.0.wait(ipcbuf).badge() }
    }

    #[cfg(not(feature = "CONFIG_KERNEL_MCS"))]
    pub fn wait(&self, ipcbuf: &mut IPCBuffer) -> usize {
        unsafe { self.0.recv(ipcbuf).badge() }
    }
}
